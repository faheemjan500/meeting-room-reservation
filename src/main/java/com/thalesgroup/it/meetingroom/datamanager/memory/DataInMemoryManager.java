/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */

package com.thalesgroup.it.meetingroom.datamanager.memory;

import com.thalesgroup.it.meetingroom.datamanager.DataManagerInterface;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.model.BaseData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataInMemoryManager<D extends BaseData> implements DataManagerInterface<D> {

  private static final Logger LOGGER = Logger.getLogger(DataInMemoryManager.class.getName());
  private final Map<Integer, D> data = new HashMap<>();

  @Override
  public D getData(Integer key) throws ItemNotFoundException {
    if (data.containsKey(key)) {
      return data.get(key);
    } else {
      throw new ItemNotFoundException("Item not found by key=" + key);
    }
  }

  @Override
  public List<D> getData(Set<Integer> keys) throws ItemNotFoundException {
    final List<D> foundItems = new ArrayList<>();
    D foundItem = null;
    for (final Integer key : keys) {
      try {
        foundItem = getData(key);
        foundItems.add(foundItem);
      } catch (final ItemNotFoundException e) {
        LOGGER.log(Level.FINEST, e.getMessage());
      }
    }
    if (foundItems.isEmpty()) {
      throw new ItemNotFoundException("No found items with provided list of keys");
    }
    return foundItems;
  }

  @Override
  public Set<Integer> getKeys() {
    return data.keySet();
  }

  @Override
  public D removeData(Integer key) {
    return data.remove(key);
  }

  @Override
  public void setData(D itemToStore) throws ItemAlreadyExistException {
    data.put(itemToStore.getKey(), itemToStore);
  }

  @Override
  public void setDatas(List<D> itemsToStore) throws ItemAlreadyExistException {
    for (final D itemToStore : itemsToStore) {
      setData(itemToStore);
    }
  }
}
