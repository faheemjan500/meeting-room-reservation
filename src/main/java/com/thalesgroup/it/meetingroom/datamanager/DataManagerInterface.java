/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */

package com.thalesgroup.it.meetingroom.datamanager;

import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;

import java.util.List;
import java.util.Set;

public interface DataManagerInterface<D> {

  D getData(Integer key) throws ItemNotFoundException;

  List<D> getData(Set<Integer> keys) throws ItemNotFoundException;

  Set<Integer> getKeys();

  D removeData(Integer key);

  void setData(D data) throws ItemAlreadyExistException, ItemNotFoundException;

  void setDatas(List<D> datas) throws ItemAlreadyExistException, ItemNotFoundException;
}
