/*
 * Copyright 2019 Thales Italia spa.
 * 
 * This program is not yet licensed and this file may not be used under any
 * circumstance.
 */

package com.thalesgroup.it.meetingroom.datamanager.exception;

public class ItemNotFoundException extends Exception {

  private static final long serialVersionUID = 1L;

  public ItemNotFoundException(String exceptionMessage) {
    super(exceptionMessage);
  }

}
