/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */

package com.thalesgroup.it.meetingroom.datamanager.memory;

import com.thalesgroup.it.meetingroom.model.Room;

public class RoomDataInMemoryManager extends DataInMemoryManager<Room> {

  private static RoomDataInMemoryManager singleInstasnce = null;

  public static RoomDataInMemoryManager getInstance() {
    if (singleInstasnce == null) {
      singleInstasnce = new RoomDataInMemoryManager();
    }
    return singleInstasnce;
  }

  private RoomDataInMemoryManager() {
  }

}
