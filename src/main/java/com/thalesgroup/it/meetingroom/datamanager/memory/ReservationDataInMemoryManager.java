/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */

package com.thalesgroup.it.meetingroom.datamanager.memory;

import com.thalesgroup.it.meetingroom.datamanager.exception.ItemAlreadyExistException;
import com.thalesgroup.it.meetingroom.datamanager.exception.ItemNotFoundException;
import com.thalesgroup.it.meetingroom.model.Reservation;

import java.util.List;
import java.util.Set;

public class ReservationDataInMemoryManager extends DataInMemoryManager<Reservation> {

  private static ReservationDataInMemoryManager singleInstance = null;

  public static ReservationDataInMemoryManager getInstance() {
    if (singleInstance == null) {
      singleInstance = new ReservationDataInMemoryManager();
    }
    return singleInstance;
  }

  private ReservationDataInMemoryManager() {
  }

  public void setReservation(Reservation itemToStore) throws ItemAlreadyExistException {
    final Set<Integer> keys = getKeys();
    List<Reservation> reservations;
    try {
      reservations = getData(keys);
      for (final Reservation reservation : reservations) {
        if (reservation.getRoomKey().equals(itemToStore.getRoomKey())
            && reservation.getStartTime().equals(itemToStore.getStartTime())
            && reservation.getEndTime().equals(itemToStore.getEndTime())) {
          throw new ItemAlreadyExistException("Item already exist");
        }
      }
    } catch (final ItemNotFoundException e) {
      // NoTODO
    }
    setData(itemToStore);

  }
}
