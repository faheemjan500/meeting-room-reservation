/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */

package com.thalesgroup.it.meetingroom.model;

import java.util.Date;

public class Reservation extends BaseData {
  private final Integer personKey;
  private final Integer roomKey;
  private final Date startTime;
  private final Date endTime;
  private final Integer reservationKey;

  public Reservation(Integer reservationKey, Integer personKey, Integer roomKey, Date startTime,
      Date endTime) {
    this.personKey = personKey;
    this.roomKey = roomKey;
    this.startTime = startTime;
    this.endTime = endTime;
    this.reservationKey = reservationKey;

  }

  public Date getEndTime() {
    return endTime;
  }

  public Integer getPersonKey() {
    return personKey;
  }

  public Integer getReservationKey() {
    return reservationKey;
  }

  public Integer getRoomKey() {
    return roomKey;
  }

  public Date getStartTime() {
    return startTime;
  }

}
