/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.thalesgroup.it.meetingroom.model;

public class BaseData {

  private Integer key;

  public BaseData() {
  }

  public BaseData(Integer key) {
    super();
    this.key = key;
  }

  public Integer getKey() {
    return key;
  }

  public void setKey(Integer key) {
    this.key = key;
  }

}