/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */

package com.thalesgroup.it.meetingroom.model;

public class Person extends BaseData implements Data {

  private String firstName;
  private String lastName;
  String name = "faheem";

  public Person(String firstName, String lastName, int key) {
    super(key);
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public String getFistName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setFirstName(String fname) {
    firstName = fname;
  }

  public void setLastName(String lname) {
    lastName = lname;
  }
}
